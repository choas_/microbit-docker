### micro:bit examples build image
###
### see also https://github.com/lancaster-university/microbit-samples
###          https://lancaster-university.github.io/microbit-docs/offline-toolchains/#yotta

FROM ubuntu:16.04

RUN apt-get update

RUN apt-get install -y srecord git
RUN git clone https://github.com/lancaster-university/microbit-samples

RUN apt-get install -y python-setuptools cmake build-essential ninja-build python-dev libffi-dev libssl-dev

RUN apt-get install -y curl
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
RUN python get-pip.py

RUN pip install yotta
RUN mkdir -p /usr/local/lib/yotta_modules
RUN chmod 755 /usr/local/lib/yotta_modules

RUN apt-get remove -y binutils-arm-none-eabi gcc-arm-none-eabi
RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:team-gcc-arm-embedded/ppa
RUN apt-get update
RUN apt-get install -y gcc-arm-embedded

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

### see also https://github.com/ARMmbed/yotta/issues/856
COPY version_py.patch .
RUN patch /usr/local/lib/python2.7/dist-packages/yotta/lib/version.py -i version_py.patch
RUN rm version_py.patch

WORKDIR microbit-samples

RUN yt target bbc-microbit-classic-gcc
RUN yt build

ENTRYPOINT /bin/bash
