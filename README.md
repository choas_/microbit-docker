# micro:bit docker image

build docker file:

```sh
docker build -t choas23/microbit .
```

run docker image:

```sh
docker run -it --name microbit choas23/microbit
```

get created micro:bit image:

```sh
docker cp microbit:/microbit-samples/build/bbc-microbit-classic-gcc/source/microbit-samples-combined.hex /Volumes/MINI/.
```

## upload to dockerhub

```sh
docker login --username=yourhubusername --password=yourpassword
docker push choas23/microbit:latest
```
